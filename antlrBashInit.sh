#!/bin/bash

export CLASSPATH=".:/d/stuff/codingStuff/IntroToAntlr/antlr-4.6-complete.jar:$CLASSPATH"
alias antlr4='java -Xmx500M -cp "./antlr-4.6-complete.jar:$CLASSPATH" org.antlr.v4.Tool'
alias grun='java -Xmx500M -cp "./antlr-4.6-complete.jar:$CLASSPATH" org.antlr.v4.gui.TestRig'
