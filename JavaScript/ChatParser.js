// Generated from Chat.g4 by ANTLR 4.6
// jshint ignore: start
var antlr4 = require('antlr4/index');
var ChatListener = require('./ChatListener').ChatListener;
var grammarFileName = "Chat.g4";

var serializedATN = ["\u0003\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd",
    "\u0003\u0010M\u0004\u0002\t\u0002\u0004\u0003\t\u0003\u0004\u0004\t",
    "\u0004\u0004\u0005\t\u0005\u0004\u0006\t\u0006\u0004\u0007\t\u0007\u0004",
    "\b\t\b\u0004\t\t\t\u0004\n\t\n\u0003\u0002\u0006\u0002\u0016\n\u0002",
    "\r\u0002\u000e\u0002\u0017\u0003\u0002\u0003\u0002\u0003\u0003\u0003",
    "\u0003\u0003\u0003\u0003\u0003\u0003\u0003\u0003\u0004\u0003\u0004\u0003",
    "\u0004\u0003\u0004\u0003\u0004\u0003\u0004\u0006\u0004\'\n\u0004\r\u0004",
    "\u000e\u0004(\u0003\u0005\u0003\u0005\u0003\u0006\u0003\u0006\u0003",
    "\u0006\u0003\u0006\u0003\u0007\u0003\u0007\u0005\u00073\n\u0007\u0003",
    "\u0007\u0003\u0007\u0003\u0007\u0005\u00078\n\u0007\u0003\u0007\u0005",
    "\u0007;\n\u0007\u0003\b\u0003\b\u0003\b\u0003\b\u0003\b\u0003\b\u0003",
    "\b\u0003\t\u0003\t\u0003\t\u0003\t\u0003\t\u0003\t\u0003\n\u0003\n\u0003",
    "\n\u0003\n\u0002\u0002\u000b\u0002\u0004\u0006\b\n\f\u000e\u0010\u0012",
    "\u0002\u0003\u0003\u0002\u000b\fM\u0002\u0015\u0003\u0002\u0002\u0002",
    "\u0004\u001b\u0003\u0002\u0002\u0002\u0006&\u0003\u0002\u0002\u0002",
    "\b*\u0003\u0002\u0002\u0002\n,\u0003\u0002\u0002\u0002\f:\u0003\u0002",
    "\u0002\u0002\u000e<\u0003\u0002\u0002\u0002\u0010C\u0003\u0002\u0002",
    "\u0002\u0012I\u0003\u0002\u0002\u0002\u0014\u0016\u0005\u0004\u0003",
    "\u0002\u0015\u0014\u0003\u0002\u0002\u0002\u0016\u0017\u0003\u0002\u0002",
    "\u0002\u0017\u0015\u0003\u0002\u0002\u0002\u0017\u0018\u0003\u0002\u0002",
    "\u0002\u0018\u0019\u0003\u0002\u0002\u0002\u0019\u001a\u0007\u0002\u0002",
    "\u0003\u001a\u0003\u0003\u0002\u0002\u0002\u001b\u001c\u0005\b\u0005",
    "\u0002\u001c\u001d\u0005\n\u0006\u0002\u001d\u001e\u0005\u0006\u0004",
    "\u0002\u001e\u001f\u0007\u000f\u0002\u0002\u001f\u0005\u0003\u0002\u0002",
    "\u0002 \'\u0005\f\u0007\u0002!\'\u0005\u000e\b\u0002\"\'\u0005\u0010",
    "\t\u0002#\'\u0005\u0012\n\u0002$\'\u0007\r\u0002\u0002%\'\u0007\u000e",
    "\u0002\u0002& \u0003\u0002\u0002\u0002&!\u0003\u0002\u0002\u0002&\"",
    "\u0003\u0002\u0002\u0002&#\u0003\u0002\u0002\u0002&$\u0003\u0002\u0002",
    "\u0002&%\u0003\u0002\u0002\u0002\'(\u0003\u0002\u0002\u0002(&\u0003",
    "\u0002\u0002\u0002()\u0003\u0002\u0002\u0002)\u0007\u0003\u0002\u0002",
    "\u0002*+\u0007\r\u0002\u0002+\t\u0003\u0002\u0002\u0002,-\t\u0002\u0002",
    "\u0002-.\u0007\u0003\u0002\u0002./\u0007\u000e\u0002\u0002/\u000b\u0003",
    "\u0002\u0002\u000202\u0007\u0003\u0002\u000213\u0007\u0004\u0002\u0002",
    "21\u0003\u0002\u0002\u000223\u0003\u0002\u0002\u000234\u0003\u0002\u0002",
    "\u00024;\u0007\u0005\u0002\u000257\u0007\u0003\u0002\u000268\u0007\u0004",
    "\u0002\u000276\u0003\u0002\u0002\u000278\u0003\u0002\u0002\u000289\u0003",
    "\u0002\u0002\u00029;\u0007\u0006\u0002\u0002:0\u0003\u0002\u0002\u0002",
    ":5\u0003\u0002\u0002\u0002;\r\u0003\u0002\u0002\u0002<=\u0007\u0007",
    "\u0002\u0002=>\u0007\u0010\u0002\u0002>?\u0007\b\u0002\u0002?@\u0007",
    "\u0006\u0002\u0002@A\u0007\u0010\u0002\u0002AB\u0007\u0005\u0002\u0002",
    "B\u000f\u0003\u0002\u0002\u0002CD\u0007\t\u0002\u0002DE\u0007\r\u0002",
    "\u0002EF\u0007\t\u0002\u0002FG\u0005\u0006\u0004\u0002GH\u0007\t\u0002",
    "\u0002H\u0011\u0003\u0002\u0002\u0002IJ\u0007\n\u0002\u0002JK\u0007",
    "\r\u0002\u0002K\u0013\u0003\u0002\u0002\u0002\b\u0017&(27:"].join("");


var atn = new antlr4.atn.ATNDeserializer().deserialize(serializedATN);

var decisionsToDFA = atn.decisionToState.map( function(ds, index) { return new antlr4.dfa.DFA(ds, index); });

var sharedContextCache = new antlr4.PredictionContextCache();

var literalNames = [ null, "':'", "'-'", "')'", "'('", "'['", "']'", "'/'", 
                     "'@'" ];

var symbolicNames = [ null, null, null, null, null, null, null, null, null, 
                      "SAYS", "SHOUTS", "WORD", "WHITESPACE", "NEWLINE", 
                      "TEXT" ];

var ruleNames =  [ "chat", "line", "message", "name", "command", "emoticon", 
                   "link", "color", "mention" ];

function ChatParser (input) {
	antlr4.Parser.call(this, input);
    this._interp = new antlr4.atn.ParserATNSimulator(this, atn, decisionsToDFA, sharedContextCache);
    this.ruleNames = ruleNames;
    this.literalNames = literalNames;
    this.symbolicNames = symbolicNames;
    return this;
}

ChatParser.prototype = Object.create(antlr4.Parser.prototype);
ChatParser.prototype.constructor = ChatParser;

Object.defineProperty(ChatParser.prototype, "atn", {
	get : function() {
		return atn;
	}
});

ChatParser.EOF = antlr4.Token.EOF;
ChatParser.T__0 = 1;
ChatParser.T__1 = 2;
ChatParser.T__2 = 3;
ChatParser.T__3 = 4;
ChatParser.T__4 = 5;
ChatParser.T__5 = 6;
ChatParser.T__6 = 7;
ChatParser.T__7 = 8;
ChatParser.SAYS = 9;
ChatParser.SHOUTS = 10;
ChatParser.WORD = 11;
ChatParser.WHITESPACE = 12;
ChatParser.NEWLINE = 13;
ChatParser.TEXT = 14;

ChatParser.RULE_chat = 0;
ChatParser.RULE_line = 1;
ChatParser.RULE_message = 2;
ChatParser.RULE_name = 3;
ChatParser.RULE_command = 4;
ChatParser.RULE_emoticon = 5;
ChatParser.RULE_link = 6;
ChatParser.RULE_color = 7;
ChatParser.RULE_mention = 8;

function ChatContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = ChatParser.RULE_chat;
    return this;
}

ChatContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
ChatContext.prototype.constructor = ChatContext;

ChatContext.prototype.EOF = function() {
    return this.getToken(ChatParser.EOF, 0);
};

ChatContext.prototype.line = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(LineContext);
    } else {
        return this.getTypedRuleContext(LineContext,i);
    }
};

ChatContext.prototype.enterRule = function(listener) {
    if(listener instanceof ChatListener ) {
        listener.enterChat(this);
	}
};

ChatContext.prototype.exitRule = function(listener) {
    if(listener instanceof ChatListener ) {
        listener.exitChat(this);
	}
};




ChatParser.ChatContext = ChatContext;

ChatParser.prototype.chat = function() {

    var localctx = new ChatContext(this, this._ctx, this.state);
    this.enterRule(localctx, 0, ChatParser.RULE_chat);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 19; 
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        do {
            this.state = 18;
            this.line();
            this.state = 21; 
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        } while(_la===ChatParser.WORD);
        this.state = 23;
        this.match(ChatParser.EOF);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function LineContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = ChatParser.RULE_line;
    return this;
}

LineContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
LineContext.prototype.constructor = LineContext;

LineContext.prototype.name = function() {
    return this.getTypedRuleContext(NameContext,0);
};

LineContext.prototype.command = function() {
    return this.getTypedRuleContext(CommandContext,0);
};

LineContext.prototype.message = function() {
    return this.getTypedRuleContext(MessageContext,0);
};

LineContext.prototype.NEWLINE = function() {
    return this.getToken(ChatParser.NEWLINE, 0);
};

LineContext.prototype.enterRule = function(listener) {
    if(listener instanceof ChatListener ) {
        listener.enterLine(this);
	}
};

LineContext.prototype.exitRule = function(listener) {
    if(listener instanceof ChatListener ) {
        listener.exitLine(this);
	}
};




ChatParser.LineContext = LineContext;

ChatParser.prototype.line = function() {

    var localctx = new LineContext(this, this._ctx, this.state);
    this.enterRule(localctx, 2, ChatParser.RULE_line);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 25;
        this.name();
        this.state = 26;
        this.command();
        this.state = 27;
        this.message();
        this.state = 28;
        this.match(ChatParser.NEWLINE);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function MessageContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = ChatParser.RULE_message;
    return this;
}

MessageContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
MessageContext.prototype.constructor = MessageContext;

MessageContext.prototype.emoticon = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(EmoticonContext);
    } else {
        return this.getTypedRuleContext(EmoticonContext,i);
    }
};

MessageContext.prototype.link = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(LinkContext);
    } else {
        return this.getTypedRuleContext(LinkContext,i);
    }
};

MessageContext.prototype.color = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(ColorContext);
    } else {
        return this.getTypedRuleContext(ColorContext,i);
    }
};

MessageContext.prototype.mention = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(MentionContext);
    } else {
        return this.getTypedRuleContext(MentionContext,i);
    }
};

MessageContext.prototype.WORD = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(ChatParser.WORD);
    } else {
        return this.getToken(ChatParser.WORD, i);
    }
};


MessageContext.prototype.WHITESPACE = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(ChatParser.WHITESPACE);
    } else {
        return this.getToken(ChatParser.WHITESPACE, i);
    }
};


MessageContext.prototype.enterRule = function(listener) {
    if(listener instanceof ChatListener ) {
        listener.enterMessage(this);
	}
};

MessageContext.prototype.exitRule = function(listener) {
    if(listener instanceof ChatListener ) {
        listener.exitMessage(this);
	}
};




ChatParser.MessageContext = MessageContext;

ChatParser.prototype.message = function() {

    var localctx = new MessageContext(this, this._ctx, this.state);
    this.enterRule(localctx, 4, ChatParser.RULE_message);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 36; 
        this._errHandler.sync(this);
        var _alt = 1;
        do {
        	switch (_alt) {
        	case 1:
        		this.state = 36;
        		this._errHandler.sync(this);
        		switch(this._input.LA(1)) {
        		case ChatParser.T__0:
        		    this.state = 30;
        		    this.emoticon();
        		    break;
        		case ChatParser.T__4:
        		    this.state = 31;
        		    this.link();
        		    break;
        		case ChatParser.T__6:
        		    this.state = 32;
        		    this.color();
        		    break;
        		case ChatParser.T__7:
        		    this.state = 33;
        		    this.mention();
        		    break;
        		case ChatParser.WORD:
        		    this.state = 34;
        		    this.match(ChatParser.WORD);
        		    break;
        		case ChatParser.WHITESPACE:
        		    this.state = 35;
        		    this.match(ChatParser.WHITESPACE);
        		    break;
        		default:
        		    throw new antlr4.error.NoViableAltException(this);
        		}
        		break;
        	default:
        		throw new antlr4.error.NoViableAltException(this);
        	}
        	this.state = 38; 
        	this._errHandler.sync(this);
        	_alt = this._interp.adaptivePredict(this._input,2, this._ctx);
        } while ( _alt!=2 && _alt!=antlr4.atn.ATN.INVALID_ALT_NUMBER );
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function NameContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = ChatParser.RULE_name;
    return this;
}

NameContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
NameContext.prototype.constructor = NameContext;

NameContext.prototype.WORD = function() {
    return this.getToken(ChatParser.WORD, 0);
};

NameContext.prototype.enterRule = function(listener) {
    if(listener instanceof ChatListener ) {
        listener.enterName(this);
	}
};

NameContext.prototype.exitRule = function(listener) {
    if(listener instanceof ChatListener ) {
        listener.exitName(this);
	}
};




ChatParser.NameContext = NameContext;

ChatParser.prototype.name = function() {

    var localctx = new NameContext(this, this._ctx, this.state);
    this.enterRule(localctx, 6, ChatParser.RULE_name);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 40;
        this.match(ChatParser.WORD);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function CommandContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = ChatParser.RULE_command;
    return this;
}

CommandContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
CommandContext.prototype.constructor = CommandContext;

CommandContext.prototype.WHITESPACE = function() {
    return this.getToken(ChatParser.WHITESPACE, 0);
};

CommandContext.prototype.SAYS = function() {
    return this.getToken(ChatParser.SAYS, 0);
};

CommandContext.prototype.SHOUTS = function() {
    return this.getToken(ChatParser.SHOUTS, 0);
};

CommandContext.prototype.enterRule = function(listener) {
    if(listener instanceof ChatListener ) {
        listener.enterCommand(this);
	}
};

CommandContext.prototype.exitRule = function(listener) {
    if(listener instanceof ChatListener ) {
        listener.exitCommand(this);
	}
};




ChatParser.CommandContext = CommandContext;

ChatParser.prototype.command = function() {

    var localctx = new CommandContext(this, this._ctx, this.state);
    this.enterRule(localctx, 8, ChatParser.RULE_command);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 42;
        _la = this._input.LA(1);
        if(!(_la===ChatParser.SAYS || _la===ChatParser.SHOUTS)) {
        this._errHandler.recoverInline(this);
        }
        else {
        	this._errHandler.reportMatch(this);
            this.consume();
        }
        this.state = 43;
        this.match(ChatParser.T__0);
        this.state = 44;
        this.match(ChatParser.WHITESPACE);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function EmoticonContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = ChatParser.RULE_emoticon;
    return this;
}

EmoticonContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
EmoticonContext.prototype.constructor = EmoticonContext;


EmoticonContext.prototype.enterRule = function(listener) {
    if(listener instanceof ChatListener ) {
        listener.enterEmoticon(this);
	}
};

EmoticonContext.prototype.exitRule = function(listener) {
    if(listener instanceof ChatListener ) {
        listener.exitEmoticon(this);
	}
};




ChatParser.EmoticonContext = EmoticonContext;

ChatParser.prototype.emoticon = function() {

    var localctx = new EmoticonContext(this, this._ctx, this.state);
    this.enterRule(localctx, 10, ChatParser.RULE_emoticon);
    var _la = 0; // Token type
    try {
        this.state = 56;
        this._errHandler.sync(this);
        var la_ = this._interp.adaptivePredict(this._input,5,this._ctx);
        switch(la_) {
        case 1:
            this.enterOuterAlt(localctx, 1);
            this.state = 46;
            this.match(ChatParser.T__0);
            this.state = 48;
            this._errHandler.sync(this);
            _la = this._input.LA(1);
            if(_la===ChatParser.T__1) {
                this.state = 47;
                this.match(ChatParser.T__1);
            }

            this.state = 50;
            this.match(ChatParser.T__2);
            break;

        case 2:
            this.enterOuterAlt(localctx, 2);
            this.state = 51;
            this.match(ChatParser.T__0);
            this.state = 53;
            this._errHandler.sync(this);
            _la = this._input.LA(1);
            if(_la===ChatParser.T__1) {
                this.state = 52;
                this.match(ChatParser.T__1);
            }

            this.state = 55;
            this.match(ChatParser.T__3);
            break;

        }
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function LinkContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = ChatParser.RULE_link;
    return this;
}

LinkContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
LinkContext.prototype.constructor = LinkContext;

LinkContext.prototype.TEXT = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(ChatParser.TEXT);
    } else {
        return this.getToken(ChatParser.TEXT, i);
    }
};


LinkContext.prototype.enterRule = function(listener) {
    if(listener instanceof ChatListener ) {
        listener.enterLink(this);
	}
};

LinkContext.prototype.exitRule = function(listener) {
    if(listener instanceof ChatListener ) {
        listener.exitLink(this);
	}
};




ChatParser.LinkContext = LinkContext;

ChatParser.prototype.link = function() {

    var localctx = new LinkContext(this, this._ctx, this.state);
    this.enterRule(localctx, 12, ChatParser.RULE_link);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 58;
        this.match(ChatParser.T__4);
        this.state = 59;
        this.match(ChatParser.TEXT);
        this.state = 60;
        this.match(ChatParser.T__5);
        this.state = 61;
        this.match(ChatParser.T__3);
        this.state = 62;
        this.match(ChatParser.TEXT);
        this.state = 63;
        this.match(ChatParser.T__2);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function ColorContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = ChatParser.RULE_color;
    return this;
}

ColorContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
ColorContext.prototype.constructor = ColorContext;

ColorContext.prototype.WORD = function() {
    return this.getToken(ChatParser.WORD, 0);
};

ColorContext.prototype.message = function() {
    return this.getTypedRuleContext(MessageContext,0);
};

ColorContext.prototype.enterRule = function(listener) {
    if(listener instanceof ChatListener ) {
        listener.enterColor(this);
	}
};

ColorContext.prototype.exitRule = function(listener) {
    if(listener instanceof ChatListener ) {
        listener.exitColor(this);
	}
};




ChatParser.ColorContext = ColorContext;

ChatParser.prototype.color = function() {

    var localctx = new ColorContext(this, this._ctx, this.state);
    this.enterRule(localctx, 14, ChatParser.RULE_color);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 65;
        this.match(ChatParser.T__6);
        this.state = 66;
        this.match(ChatParser.WORD);
        this.state = 67;
        this.match(ChatParser.T__6);
        this.state = 68;
        this.message();
        this.state = 69;
        this.match(ChatParser.T__6);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function MentionContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = ChatParser.RULE_mention;
    return this;
}

MentionContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
MentionContext.prototype.constructor = MentionContext;

MentionContext.prototype.WORD = function() {
    return this.getToken(ChatParser.WORD, 0);
};

MentionContext.prototype.enterRule = function(listener) {
    if(listener instanceof ChatListener ) {
        listener.enterMention(this);
	}
};

MentionContext.prototype.exitRule = function(listener) {
    if(listener instanceof ChatListener ) {
        listener.exitMention(this);
	}
};




ChatParser.MentionContext = MentionContext;

ChatParser.prototype.mention = function() {

    var localctx = new MentionContext(this, this._ctx, this.state);
    this.enterRule(localctx, 16, ChatParser.RULE_mention);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 71;
        this.match(ChatParser.T__7);
        this.state = 72;
        this.match(ChatParser.WORD);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


exports.ChatParser = ChatParser;
