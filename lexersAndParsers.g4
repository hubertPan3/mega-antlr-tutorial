/*
* Rule Syntax: "<name> : <rule_definition>;"
*
*/

/*
 * Parser Rules
 * Written first, labeled with only lowercase (technically only first char matters)
 */
   
operation  : NUMBER '+' NUMBER ;
    
/*
* Lexer Rules
* Written second, labeled only uppercase (technically only first char matters)
* Lexer rules analyzed in order of appearance, so keyword lexers should go first
* e.g. "public"
*/
       
NUMBER     : [0-9]+ ;
        
WHITESPACE : ' ' -> skip ;
